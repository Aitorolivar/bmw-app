import { Component, OnInit } from '@angular/core';
import { ItodoCaminos } from '../../models/itodo-caminos';

@Component({
  selector: 'app-todo-caminos-view',
  templateUrl: './todo-caminos-view.component.html',
  styleUrls: ['./todo-caminos-view.component.scss']
})
export class TodoCaminosViewComponent implements OnInit {

  public todoView!: ItodoCaminos;
  constructor() {
  }

  ngOnInit(): void {
  }

}
