import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoCaminosPageRoutingModule } from './todo-caminos-page-routing.module';
import { TodoCaminosViewComponent } from './components/todo-caminos-view/todo-caminos-view.component';
import { TodoCaminoInitComponent } from './components/todo-camino-init/todo-camino-init.component';
import { TodoCaminoMidComponent } from './components/todo-camino-mid/todo-camino-mid.component';


@NgModule({
  declarations: [
    TodoCaminosViewComponent,
    TodoCaminoInitComponent,
    TodoCaminoMidComponent
  ],
  imports: [
    CommonModule,
    TodoCaminosPageRoutingModule
  ]
})
export class TodoCaminosPageModule { }
