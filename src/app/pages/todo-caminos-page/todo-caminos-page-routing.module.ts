import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodoCaminosViewComponent } from './components/todo-caminos-view/todo-caminos-view.component';

const routes: Routes = [
  {
    path: '',
    component: TodoCaminosViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoCaminosPageRoutingModule { }
