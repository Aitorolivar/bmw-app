import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BerlinaPageRoutingModule } from './berlina-page-routing.module';
import { BerlinaViewComponent } from './components/berlina-view/berlina-view.component';


@NgModule({
  declarations: [
    BerlinaViewComponent
  ],
  imports: [
    CommonModule,
    BerlinaPageRoutingModule
  ]
})
export class BerlinaPageModule { }
