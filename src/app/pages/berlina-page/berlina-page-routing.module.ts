import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BerlinaViewComponent } from './components/berlina-view/berlina-view.component';

const routes: Routes = [
  {
    path:"",
    component: BerlinaViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BerlinaPageRoutingModule { }
