import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeportivosPageRoutingModule } from './deportivos-page-routing.module';
import { DeportivosViewComponent } from './components/deportivos-view/deportivos-view.component';


@NgModule({
  declarations: [
    DeportivosViewComponent
  ],
  imports: [
    CommonModule,
    DeportivosPageRoutingModule
  ]
})
export class DeportivosPageModule { }
