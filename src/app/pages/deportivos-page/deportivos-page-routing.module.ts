import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeportivosViewComponent } from './components/deportivos-view/deportivos-view.component';

const routes: Routes = [
  {
    path:"",
    component: DeportivosViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeportivosPageRoutingModule { }
