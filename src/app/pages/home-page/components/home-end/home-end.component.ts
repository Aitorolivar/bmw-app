import { Component, Input, OnInit } from '@angular/core';
import { IhomeEnd } from '../../models/ihome';

@Component({
  selector: 'app-home-end',
  templateUrl: './home-end.component.html',
  styleUrls: ['./home-end.component.scss']
})
export class HomeEndComponent implements OnInit {

  @Input() public homeEnd! : IhomeEnd;
  constructor() { }

  ngOnInit(): void {
  }

}
