import { Component, Input, OnInit } from '@angular/core';
import { IhomeInit } from '../../models/ihome';

@Component({
  selector: 'app-home-init',
  templateUrl: './home-init.component.html',
  styleUrls: ['./home-init.component.scss']
})
export class HomeInitComponent implements OnInit {

  @Input() public homeInit! : IhomeInit;
  
  constructor() { }

  ngOnInit(): void {
  }

}
