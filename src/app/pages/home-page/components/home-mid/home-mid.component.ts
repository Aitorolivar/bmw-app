import { Component, Input, OnInit } from '@angular/core';
import { IhomeMid } from '../../models/ihome';

@Component({
  selector: 'app-home-mid',
  templateUrl: './home-mid.component.html',
  styleUrls: ['./home-mid.component.scss']
})
export class HomeMidComponent implements OnInit {

  @Input() public homeMid!: IhomeMid;
  constructor() { }

  ngOnInit(): void {
  }

}
