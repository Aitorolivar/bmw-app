import { Component, OnInit } from '@angular/core';
import { Ihome } from '../../models/ihome';

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.scss']
})
export class HomeViewComponent implements OnInit {

  public homeView!: Ihome;

  constructor() {
    this.homeView = {

      homeInit: {
      boxInit: [
        {
          text: "NUEVO BMW SERIE 2",
          botton: "Mantenme Informado"
        },
        {
          text: "VEHÍCULOS DISPONIBLES",
          botton: "Encuentra tu BMW"
        }
      ]
      },

      homeMid: {
        boxMid: [
          {
            img: {
              src: '../../../../assets/image/imghome/home-Mid/card-mid1.jpg',
              alt: 'bmw'
            },
            tittleMid:'PROMOCIONES BMW EMPRESAS',
            textMid1: 'Descubre las ofertas y opciones en modelos BMW para tu empresa',
            textMid2: '> Ver promociones'
          },
          {
            img: {
              src: '../../../../assets/image/imghome/home-Mid/card-mid2.jpg',
              alt: 'bmw'
            },
            tittleMid:'VEHÍCULO NUEVO DISPONIBLE',
            textMid1: 'Encuentra tu BMW',
            textMid2: '> Ver modelos'
          },
          {
            img: {
              src: '../../../../assets/image/imghome/home-Mid/card-mid3.jpg',
              alt: 'bmw'
            },
            tittleMid:'VEHÍCULOS DE OCASIÓN',
            textMid1: 'Descubre una amplia gama de vehículos de ocasión BMW',
            textMid2: '> Descubre la gama'
          },
          {
            img: {
              src: '../../../../assets/image/imghome/home-Mid/card-mid4.jpg',
              alt: 'bmw'
            },
            tittleMid:'ACCESORIOS ORIGINALES BMW',
            textMid1: 'Conviertelo en tu verano',
            textMid2: '> Descúbrelos'
          }
        ]
      },

      homeEnd: {
        boxEnd: [
          {
            img: {
              src: '../../../../assets/image/imghome/home-End/8731-bmw.jpg',
              alt: 'bmw'
            },
            tittleEnd: 'Configurar tu BMW individual',
            textEnd: '> Personaliza tu modelo'
          },
          {
            img: {
              src: '../../../../assets/image/imghome/home-End/8741-bmw.jpg',
              alt: 'bmw'
            },
            tittleEnd: 'BMW M135i xDrive Performance',
            textEnd: '> Descubre la edición más exclusiva'
          }
        ]
      }
    }
  }

  ngOnInit(): void {
  }

}




