import { IboxEnd, IboxInit, IboxMid } from "src/app/models/iglobal";

export interface Ihome {
    homeInit: IhomeInit;
    homeMid: IhomeMid;
    homeEnd: IhomeEnd; 
}

export interface IhomeInit {
    boxInit: IboxInit[];
}

export interface IhomeMid {
    boxMid: IboxMid[];
}

export interface IhomeEnd {
    boxEnd: IboxEnd[];
}

