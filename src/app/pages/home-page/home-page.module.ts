import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageRoutingModule } from './home-page-routing.module';
import { HomeViewComponent } from './components/home-view/home-view.component';
import { HomeInitComponent } from './components/home-init/home-init.component';
import { HomeMidComponent } from './components/home-mid/home-mid.component';
import { HomeEndComponent } from './components/home-end/home-end.component';


@NgModule({
  declarations: [
    HomeViewComponent,
    HomeInitComponent,
    HomeMidComponent,
    HomeEndComponent
  ],
  imports: [
    CommonModule,
    HomePageRoutingModule
  ]
})
export class HomePageModule { }
