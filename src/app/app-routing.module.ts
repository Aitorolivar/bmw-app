import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home-page/home-page.module').then( module => module.HomePageModule)
  },
  {
    path: 'todocaminos',
    loadChildren: () => import('./pages/todo-caminos-page/todo-caminos-page.module').then( module => module.TodoCaminosPageModule)
  },
  {
    path: 'deportivos',
    loadChildren: () => import('./pages/deportivos-page/deportivos-page.module').then(module => module.DeportivosPageModule)
  },
  {
    path:'berlina',
    loadChildren: () => import('./pages/berlina-page/berlina-page.module').then(module => module.BerlinaPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// {
//
// {
//   path: 'saiyans',
//   loadChildren: () => import('./pages/saiyan-page/saiyan-page.module').then( module => module.SaiyanPageModule)
// },
// {
//   path: 'protagonists',
//   loadChildren: () => import('./pages/protagonist-page/protagonist-page.module').then( module => module.ProtagonistPageModule)
// },
// {
//   path: 'villains',
//   loadChildren: () => import('./pages/villains-page/villains-page.module').then( module => module.VillainsPageModule)
// },
// {
//   path: 'other universes',
//   loadChildren: () => import('./pages/other-universes-page/other-universes-page.module').then( module => module.OtherUniversesPageModule)
// }
