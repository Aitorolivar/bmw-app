export interface Iglobal {
}

export interface Iheader {
    name: string;
    navigate: string;
}

export interface Iimg{
    src: string;
    alt: string;
}

export interface IboxInit {
    text: string;
    botton: string;
}

export interface IboxMid{
    img: Iimg;
    tittleMid: string;
    textMid1: string;
    textMid2: string;
}

export interface IboxEnd{
    img: Iimg;
    tittleEnd: string;
    textEnd: string
}

export interface Itittle{
    tittle: string,
    text: string
}

export interface Igallery{
    carModel: string;
    img: Iimg;
}

