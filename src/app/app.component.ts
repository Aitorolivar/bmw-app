import { Component } from '@angular/core';
import { Iheader } from './models/iglobal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
 public header: Iheader[] = [
  {
  name: 'Home',
  navigate: '/home'
  },
  {
  name: 'Todo Caminos',
  navigate: '/todocaminos'
  },
  {
  name: 'Deportivos',
  navigate: '/deportivos'
  },
  {
  name: 'Berlina',
  navigate: '/berlina'
  }
]
}

